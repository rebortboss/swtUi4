package com.ztesoft.ip.ui;

import java.io.File;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import jodd.util.StringUtil;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;

import com.xiaoleilu.hutool.CharsetUtil;
import com.xiaoleilu.hutool.CronUtil;
import com.xiaoleilu.hutool.FileUtil;
import com.xiaoleilu.hutool.Log;
import com.xiaoleilu.hutool.Setting;
import com.xiaoleilu.hutool.StrUtil;
import com.xiaoleilu.hutool.SystemUtil;
import com.xiaoleilu.hutool.db.DbUtil;
import com.xiaoleilu.hutool.db.Entity;
import com.xiaoleilu.hutool.db.ds.DruidDS;
import com.xiaoleilu.hutool.db.handler.EntityListHandler;
import com.xiaoleilu.hutool.db.sql.SqlExecutor;
import com.ztesoft.ip.jobs.Backoracle;
import com.ztesoft.ip.update.DownFile;
import com.ztesoft.ip.utils.BatUtils;
import com.ztesoft.ip.utils.DialogUtil;
import com.ztesoft.ip.utils.IpAddress;
import com.ztesoft.ip.utils.LayoutUtils;
import com.ztesoft.ip.utils.PropertiesUtil;
import com.ztesoft.ip.utils.layout.SWTResourceManager;
import com.ztesoft.ip.webservice.SwtUiWeb;

public class MainApp {
	private final static Logger log = Log.get(MainApp.class.getName());
	protected Object result;
	protected static Composite shell;
	private static StyledText ip;
	private static StyledText ym;
	private static StyledText wg;
	protected static Shell shello;
	private static StyledText dns1;
	private static StyledText dns2;
	private static String namestr;
	private static String dns2str;
	private static String dns1str;
	private static String wgstr;
	private static String ymstr;
	private static String ipstr;
	private static Combo name;
	private static Text txtipdns;

	private static Integer systemtype;// 1 window 2 linux
	private static Text text;
	private static Label jobstate;
	private static StyledText db;
	private static StyledText user;
	private static StyledText passwd;
	private static Text txtDback;
	private static int jobstatenum = 0;// 0stop 1running
	private static String dbn;
	private static String dbname;
	private static String dbpass;
	private static String dbback;
	private static Label handsytxt;
	private static Button button_6;
	private static Button btnWeb;
	private static int ciwebstatus = 0;// ciweb服务启动状态0关闭 1启动

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			Display display = Display.getDefault();
			shello = new Shell(display, 0);
			shello.setSize(636, 435);
			shello.setLayout(new FormLayout());
			{
				PropertiesUtil.load("resouce/config", "value.config");
				String os = SystemUtil.get(SystemUtil.OS_NAME).toLowerCase();
				if (os.contains("win")) {
					systemtype = 1;
				} else {
					systemtype = 2;
				}

				String version = PropertiesUtil.getProperty("version");
				// String appname = PropertiesUtil.getProperty("appname");
				//
				// 中文文件加载
				Setting s = new Setting(new File("resouce/config/value.setting"), Setting.DEFAULT_CHARSET, false);

				String appname = s.getString("appname");

				shell = LayoutUtils.centerDWdefult(shello, appname + version, true, true);
				String appico = PropertiesUtil.getProperty("appico");
				if (StrUtil.isEmpty(appico)) {
					appico = "/pictures/png-0006.png";
				}
				shello.setImage(SWTResourceManager.getImage(MainApp.class, appico));

			}

			CTabFolder tabFolder = new CTabFolder(shell, SWT.BORDER);
			FormData fd_tabFolder = new FormData();
			fd_tabFolder.left = new FormAttachment(0);
			fd_tabFolder.right = new FormAttachment(100);
			fd_tabFolder.top = new FormAttachment(0);
			fd_tabFolder.bottom = new FormAttachment(100);
			tabFolder.setLayoutData(fd_tabFolder);
			tabFolder.setSelectionBackground(Display.getCurrent().getSystemColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND_GRADIENT));

			CTabItem tabItem = new CTabItem(tabFolder, SWT.NONE);
			tabItem.setText("自动获取");

			Composite composite = new Composite(tabFolder, SWT.NONE);
			tabItem.setControl(composite);
			composite.setLayout(new FormLayout());

			Label lblip = new Label(composite, SWT.NONE);
			FormData fd_lblip = new FormData();
			fd_lblip.left = new FormAttachment(0, 192);
			fd_lblip.right = new FormAttachment(100, -183);
			fd_lblip.top = new FormAttachment(0, 26);
			lblip.setLayoutData(fd_lblip);
			lblip.setText("仅Windows可用！设置网络为自动获取ip！");

			txtipdns = new Text(composite, SWT.BORDER);
			txtipdns.setText("1.适合局域网自动获取   自动获取dns");
			FormData fd_txtipdns = new FormData();
			fd_txtipdns.top = new FormAttachment(lblip, 160);
			fd_txtipdns.right = new FormAttachment(100, -166);
			fd_txtipdns.left = new FormAttachment(0, 193);
			txtipdns.setLayoutData(fd_txtipdns);

			Button button_3 = new Button(composite, SWT.NONE);
			fd_txtipdns.bottom = new FormAttachment(button_3, -31);
			button_3.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					dosetauto();
				}
			});
			FormData fd_button_3 = new FormData();
			fd_button_3.right = new FormAttachment(100, -284);
			fd_button_3.bottom = new FormAttachment(100, -66);
			button_3.setLayoutData(fd_button_3);
			button_3.setText("设 置");

			CTabItem tabItem_1 = new CTabItem(tabFolder, SWT.NONE);
			tabItem_1.setText("手动获取");

			Composite composite_1 = new Composite(tabFolder, SWT.NONE);
			tabItem_1.setControl(composite_1);
			composite_1.setLayout(null);

			CLabel lblIp = new CLabel(composite_1, SWT.NONE);
			lblIp.setBounds(180, 54, 70, 23);
			lblIp.setText("ip 地址:");

			CLabel label = new CLabel(composite_1, SWT.NONE);
			label.setBounds(180, 103, 70, 23);
			label.setText("子网掩码:");

			CLabel label_1 = new CLabel(composite_1, SWT.NONE);
			label_1.setBounds(180, 147, 70, 23);
			label_1.setText("默认网关:");

			CLabel lbldns = new CLabel(composite_1, SWT.NONE);
			lbldns.setBounds(180, 195, 70, 23);
			lbldns.setText("首选DNS:");

			CLabel lbldns_1 = new CLabel(composite_1, SWT.NONE);
			lbldns_1.setBounds(180, 242, 70, 23);
			lbldns_1.setText("备用DNS:");

			Button button = new Button(composite_1, SWT.NONE);
			button.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					dosethand();
				}
			});
			button.setBounds(299, 299, 40, 27);
			button.setText("设 置");

			ip = new StyledText(composite_1, SWT.BORDER);
			ip.addFocusListener(new FocusListener() {

				@Override
				public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
					if (IpAddress.isIP(ip.getText())) {
						if (!StringUtil.equals(ipstr, ip.getText().trim())) {
							ipstr = ip.getText().trim();

						}
					} else {
						MessageDialog.openWarning(shello, "ip格式不对", "ip格式不对");
						return;
					}
				}

				@Override
				public void focusGained(FocusEvent arg0) {
					// TODO Auto-generated method stub

				}
			});

			ip.setBounds(259, 56, 180, 21);

			ym = new StyledText(composite_1, SWT.BORDER);
			ym.setEditable(false);
			ym.setAlignment(SWT.CENTER);
			ym.setBounds(259, 105, 180, 21);

			wg = new StyledText(composite_1, SWT.BORDER);
			wg.setEditable(false);
			wg.setAlignment(SWT.CENTER);
			wg.setBounds(259, 149, 180, 21);

			dns1 = new StyledText(composite_1, SWT.BORDER);
			dns1.setEditable(false);
			dns1.setAlignment(SWT.CENTER);
			dns1.setBounds(259, 197, 180, 21);

			dns2 = new StyledText(composite_1, SWT.BORDER);
			dns2.setEditable(false);
			dns2.setAlignment(SWT.CENTER);
			dns2.setBounds(259, 244, 180, 21);

			CLabel label_2 = new CLabel(composite_1, SWT.NONE);
			label_2.setText("网络名称");
			label_2.setBounds(180, 10, 70, 23);

			name = new Combo(composite_1, SWT.NONE);
			name.setBounds(259, 10, 180, 25);

			Label lblwindows = new Label(composite_1, SWT.NONE);
			lblwindows.setBounds(31, 13, 143, 17);
			lblwindows.setText("仅Windows可用!");

			CTabItem tbtmOracle_1 = new CTabItem(tabFolder, SWT.NONE);
			tbtmOracle_1.setText("oracle定时自动任务");

			Composite composite_3 = new Composite(tabFolder, SWT.NONE);
			tbtmOracle_1.setControl(composite_3);
			composite_3.setLayout(new GridLayout(2, false));

			Label lbllinuxoracle = new Label(composite_3, SWT.NONE);
			GridData gd_lbllinuxoracle = new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1);
			gd_lbllinuxoracle.widthHint = 189;
			lbllinuxoracle.setLayoutData(gd_lbllinuxoracle);
			lbllinuxoracle.setText("1.oracle数据库自动备份;2.oracle定时自动同步.");

			Label lblNewLabel = new Label(composite_3, SWT.NONE);
			GridData gd_lblNewLabel = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
			gd_lblNewLabel.widthHint = 122;
			lblNewLabel.setLayoutData(gd_lblNewLabel);
			lblNewLabel.setText("数据库监听实例");

			db = new StyledText(composite_3, SWT.BORDER);
			db.setTopMargin(8);
			db.setAlignment(SWT.CENTER);
			db.setText("ORCLSQINNER3");
			GridData gd_db = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
			gd_db.heightHint = 38;
			db.setLayoutData(gd_db);

			Label lblNewLabel_1 = new Label(composite_3, SWT.NONE);
			lblNewLabel_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
			lblNewLabel_1.setText("用户名");

			user = new StyledText(composite_3, SWT.BORDER);
			user.setTopMargin(5);
			user.setAlignment(SWT.CENTER);
			user.setText("ycsmart2");
			GridData gd_user = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
			gd_user.heightHint = 38;
			user.setLayoutData(gd_user);

			Label lblNewLabel_2 = new Label(composite_3, SWT.NONE);
			lblNewLabel_2.setText("密码");

			passwd = new StyledText(composite_3, SWT.BORDER);
			passwd.setTopMargin(8);
			passwd.setAlignment(SWT.CENTER);
			passwd.setText("ycsmart2");
			GridData gd_passwd = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
			gd_passwd.heightHint = 41;
			passwd.setLayoutData(gd_passwd);

			Label label_4 = new Label(composite_3, SWT.NONE);
			label_4.setText("备份时间");

			text = new Text(composite_3, SWT.BORDER);
			text.setEditable(false);
			text.setText("每天24:00整进行自动备份!");
			text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

			Label label_3 = new Label(composite_3, SWT.NONE);
			label_3.setText("备份路径");

			txtDback = new Text(composite_3, SWT.BORDER);
			txtDback.setText("D:\\\\back");
			txtDback.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

			Button button_5 = new Button(composite_3, SWT.NONE);
			button_5.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					dosavedbconfig();
				}
			});
			button_5.setText("保存配置");

			Button button_1 = new Button(composite_3, SWT.NONE);
			button_1.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					doautobackjob();
				}
			});
			GridData gd_button_1 = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
			gd_button_1.widthHint = 50;
			button_1.setLayoutData(gd_button_1);
			button_1.setText("执行");
			new Label(composite_3, SWT.NONE);

			Button button_2 = new Button(composite_3, SWT.NONE);
			button_2.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					dostopjob();
				}
			});
			GridData gd_button_2 = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
			gd_button_2.widthHint = 49;
			button_2.setLayoutData(gd_button_2);
			button_2.setText("停止");
			new Label(composite_3, SWT.NONE);

			jobstate = new Label(composite_3, SWT.NONE);
			GridData gd_jobstate = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
			gd_jobstate.heightHint = 22;
			jobstate.setLayoutData(gd_jobstate);

			CTabItem tbtmOracle = new CTabItem(tabFolder, SWT.NONE);
			tbtmOracle.setText("oracle手动管理工具");

			Composite composite_4 = new Composite(tabFolder, SWT.NONE);
			tbtmOracle.setControl(composite_4);

			button_6 = new Button(composite_4, SWT.NONE);
			button_6.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					doconnectoracl();
				}
			});
			button_6.setBounds(49, 52, 101, 27);
			button_6.setText("手动同步数据库");

			handsytxt = new Label(composite_4, SWT.NONE);
			handsytxt.setBackground(SWTResourceManager.getColor(SWT.COLOR_TITLE_BACKGROUND_GRADIENT));
			handsytxt.setBounds(166, 52, 208, 27);

			CTabItem tabItem_2 = new CTabItem(tabFolder, SWT.NONE);
			tabItem_2.setText("系统管理");

			Composite composite_2 = new Composite(tabFolder, SWT.NONE);
			tabItem_2.setControl(composite_2);
			composite_2.setLayout(null);

			Button button_4 = new Button(composite_2, SWT.NONE);
			button_4.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					doupdate();
				}
			});
			button_4.setBounds(269, 226, 80, 27);
			button_4.setText("检查更新");

			Label lblQq = new Label(composite_2, SWT.NONE);
			lblQq.setBounds(44, 165, 489, 17);
			lblQq.setText("autoDeal");

			Label lblSwtRcp = new Label(composite_2, SWT.NONE);
			lblSwtRcp.setBounds(44, 188, 394, 17);
			lblSwtRcp.setText("自动化运维");

			btnWeb = new Button(composite_2, SWT.NONE);
			btnWeb.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					startorshutdownWeb();
				}
			});
			btnWeb.setText("服务已关闭,点击可启动...");
			btnWeb.setBounds(33, 24, 194, 27);

			initDate();
			shello.open();
			shello.layout();

			while (!shello.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	protected static void startorshutdownWeb() {
		// TODO Auto-generated method stub
		try {
			if (ciwebstatus == 0) {
				SwtUiWeb.startup();
				ciwebstatus = 1;
				btnWeb.setText("服务已启动,点击可关闭...");
			} else {
				SwtUiWeb.stop();
				ciwebstatus = 0;
				btnWeb.setText("服务已关闭,点击可启动...");
			}

		} catch (Exception e) {
			// TODO: handle exception
			log.error("ciweb启动失败", e);
		}
	}

	protected static void doconnectoracl() {
		button_6.setEnabled(false);
		log.info("手动同步数据库任务启动...");
		handsytxt.setText("手动同步数据库任务启动...");
		DataSource ds = null;
		DruidDS.init(new Setting(new File("resouce/config/druid.setting"), Setting.DEFAULT_CHARSET, false), new Setting(new File("resouce/config/db.setting"), Setting.DEFAULT_CHARSET, false));
		ds = DruidDS.getDataSource("oracl");
		Connection conn = null;
		CallableStatement c = null;
		try {
			conn = ds.getConnection();
			File f = new File("resouce\\config\\calls.config");
			List<String> calls = FileUtil.readLines(f.getAbsolutePath(), CharsetUtil.UTF_8);
			for (String s : calls) {
				if (null != s && s.length() > 0) {
					log.info("sql:" + s);
					c = conn.prepareCall("{" + s + "}");
					c.execute();
				}
			}

			List<Entity> entityList = com.xiaoleilu.hutool.db.sql.SqlExecutor.query(conn, "select * from DATA_PROCESS", new EntityListHandler());
			List<String> tables = new ArrayList<String>();
			for (Entity e : entityList) {
				tables.add(e.getStr("TABLE_NAME"));
			}
			for (String s : tables) {
				log.info("开始读取并更新表数据量" + s);
				List<Entity> count = SqlExecutor.query(conn, "select * from " + s, new EntityListHandler());
				if (null != count && count.size() > 0) {
					int co = count.size();
					// int num = SqlExecutor.execute(conn,
					// "UPDATE  DATA_PROCESS set cloumn_count = ?,insert_time=to_date(sysdate,'yyyy-mm-dd hh24:mi:ss'),process_state=1,process_time=to_date(sysdate,'yyyy-mm-dd hh24:mi:ss') where table_name = ?",
					// co, s);
					int num = SqlExecutor.execute(conn, "UPDATE  DATA_PROCESS set cloumn_count = ? ,insert_time=sysdate,process_state=1,process_time=sysdate where table_name = ?", co, s);
				}
			}

			log.info("手动同步数据库任务全部完成!");
			handsytxt.setText("手动同步数据库任务全部完成!");
		} catch (SQLException | IOException e) {
			Log.error(log, e, "SQL error!");
			log.info("手动同步数据库任务执行异常!");
			handsytxt.setText("手动同步数据库任务执行异常!");
		} finally {
			DbUtil.close(conn);
			button_6.setEnabled(true);
		}
	}

	protected static void dostopjob() {
		// TODO Auto-generated method stub
		if (jobstatenum == 0) {
			jobstate.setText("定时任务已经在停止,请勿重复操作!");
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			jobstate.setText("定时任务被停止...");
			return;
		} else {

			System.out.println("停止定时任务");
			CronUtil.stop();
			jobstate.setText("定时任务被停止...");
			jobstatenum = 0;
		}

	}

	protected static void doautobackjob() {
		// TODO Auto-generated method stub
		if (jobstatenum == 0) {
			System.out.println("开始定时任务");
			Backoracle.db = db.getText().trim();
			Backoracle.user = user.getText().trim();
			Backoracle.pass = passwd.getText().trim();
			/*
			 * String b = DateUtil.now().replace(":", "-").replace(" ", "");
			 * Backoracle.path = txtDback.getText().trim() + "\\" + b + ".dmp";
			 */
			Backoracle.path = txtDback.getText().trim();
			File f = new File("resouce\\config\\cron4j.setting");
			System.out.println("afa:" + f.getAbsolutePath());
			CronUtil.setCronSetting(new Setting(f, Setting.DEFAULT_CHARSET, false));
			CronUtil.start();
			jobstatenum = 1;
			jobstate.setText("定时任务运行中...");
		} else {
			jobstate.setText("定时任务已经在运行中,请勿重复操作!");
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			jobstate.setText("定时任务运行中...");
			return;
		}
	}

	protected static void doupdate() {
		// TODO Auto-generated method stub
		DownFile f = new DownFile(shello, 0);
		f.open();
	}

	protected static void dosetauto() {
		// TODO Auto-generated method stub
		if (systemtype == 2) {
			com.ztesoft.ip.utils.DialogUtil.openWarning(shello, "当前系统不是Windows平台,不可用!");
			return;
		} else {
			System.out.println(systemtype);
		}
		BatUtils.runAuto(namestr);
		com.ztesoft.ip.utils.DialogUtil.openInfo(shello, "自动获取ip dns设置成功!");
	}

	protected static void dosethand() {
		// TODO Auto-generated method stub
		if (systemtype == 2) {
			com.ztesoft.ip.utils.DialogUtil.openWarning(shello, "当前系统不是Windows平台,不可用!");
			return;
		}
		if (!IpAddress.isIP(ip.getText())) {
			ip.setText("");
			ip.setToolTipText("请输入准确的ip");
			ip.forceFocus();
			return;
		}
		BatUtils.runHandSet(namestr, ipstr, ymstr, wgstr, dns1str, dns2str);
		com.ztesoft.ip.utils.DialogUtil.openInfo(shello, "手动获取ip dns设置成功,您现在ip：" + ipstr);
	}

	private static void initDate() {
		// TODO Auto-generated method stub

		PropertiesUtil.load("resouce/config", "value.config");

		namestr = PropertiesUtil.getProperty("name");
		// #1 无线网络连接 2 本地连接
		if (StringUtil.equals(namestr, "1")) {
			namestr = "无线网络连接";
		}
		if (StringUtil.equals(namestr, "2")) {
			namestr = "本地连接";
		}
		ipstr = PropertiesUtil.getProperty("ip");
		ymstr = PropertiesUtil.getProperty("ym");
		wgstr = PropertiesUtil.getProperty("wg");
		dns1str = PropertiesUtil.getProperty("dns1");
		dns2str = PropertiesUtil.getProperty("dns2");

		dbn = PropertiesUtil.getProperty("db");
		dbname = PropertiesUtil.getProperty("dbname");
		dbpass = PropertiesUtil.getProperty("passwd");
		dbback = PropertiesUtil.getProperty("filepath");
		// 2015年10月9日 15:22:45

		name.setText(namestr);
		ip.setText(ipstr);
		ym.setText(ymstr);
		wg.setText(wgstr);
		dns1.setText(dns1str);
		dns2.setText(dns2str);

		db.setText(dbn);
		user.setText(dbname);
		passwd.setText(dbpass);
		txtDback.setText(dbback);
	}

	protected static void dosavedbconfig() {
		// TODO Auto-generated method stub
		if (txtDback.getText().trim().length() < 1 || txtDback.getText().trim().contains(" ")) {
			DialogUtil.openWarning(shello, "数据库备份目录不能为空或者有空格字符!");
			return;
		}
		PropertiesUtil.setProperty("db", db.getText().trim());
		PropertiesUtil.setProperty("dbname", user.getText().trim());
		PropertiesUtil.setProperty("passwd", passwd.getText().trim());
		PropertiesUtil.setProperty("filepath", txtDback.getText().trim());
		DialogUtil.openInfo(shello, "数据库配置信息更新成功!");
	}
}

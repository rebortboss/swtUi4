package com.ztesoft.ip.entity;

public class VoZhaiYan {
	/*
	 * {"id":"1243","zhaiyan":"我还没来得及 还没来得及想好 得到的却注定失去 还是一开始就不曾发" +
	 * "出?","cat":"b","catname":"漫画","author":null,"show":"夏" + "" +
	 * "达","source":"子不语","date":"2015-04-21","img":null}
	 */
	private String id;
	private String representative;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRepresentative() {
		return representative;
	}

	public void setRepresentative(String representative) {
		this.representative = representative;
	}

}
